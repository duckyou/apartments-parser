# -*- coding: utf-8 -*-

import time
import datetime

import schedule
import requests


def job_check_apartments():
    res_realtby = requests.post(
        'http://localhost:6800/schedule.json',
        data={'project': 'default', 'spider': 'realtby'},
    )
    print(f'[{datetime.datetime.now():%Y-%m-%d %H:%M}] - [{res_realtby.status_code}] realt.by')

    res_gohomeby = requests.post(
        'http://localhost:6800/schedule.json',
        data={'project': 'default', 'spider': 'gohomeby'},
    )
    print(f'[{datetime.datetime.now():%Y-%m-%d %H:%M}] - [{res_gohomeby.status_code}] gohome.by')

    res_kufarby = requests.post(
        'http://localhost:6800/schedule.json',
        data={'project': 'default', 'spider': 'kufarby'},
    )
    print(f'[{datetime.datetime.now():%Y-%m-%d %H:%M}] - [{res_kufarby.status_code}] kufar.by')


if __name__ == '__main__':
    # TODO: simple cli
    schedule.every(15).minutes.do(job_check_apartments)

    print('Schedule running...')
    job_check_apartments()
    try:
        while True:
            schedule.run_pending()
            time.sleep(60)
    except KeyboardInterrupt:
        print('Schedule interrupted')
