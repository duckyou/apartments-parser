# -*- coding: utf-8 -*-

import scrapy
from scrapy.http import Response
from w3lib.html import remove_tags

from ..items import ApartmentItem


class GohomebySpider(scrapy.Spider):
    name = 'gohomeby'

    def start_requests(self):
        yield scrapy.FormRequest(
            url='https://gohome.by/rent/search',
            callback=self.parse,
            formdata={
                "search[type]": "3",
                "search[link_edit]": "full",
                "search[global_region]": "6",
                "search[state]": "70",
                "search[region]": "6",
                "search[city_name]": "",
                "search[areas][]": "0",
                "search[street]": "",
                "search[room_from]": "1",
                "search[room_to]": "4",
                "search[cost_from]": "",
                "search[cost_to]": "",
                "search[sq_all_from]": "",
                "search[sq_all_to]": "",
                "search[sq_zhil_from]": "",
                "search[sq_zhil_to]": "",
                "search[sq_kuh_from]": "",
                "search[sq_kuh_to]": "",
                "search[floor_from]": "",
                "search[floor_to]": "",
                "search[floor_all_from]": "",
                "search[floor_all_to]": "",
                "search[year_from]": "",
                "search[year_to]": "",
                "search[is_sanuzel]": "-1",
                "search[is_phone]": "-1",
                "search[date_create]": "0",
                "search[sort_field]": "date_last_up_desc",
            },
        )

    def parse(self, response: Response):
        for item in response.css('table tbody tr.table-adveadverts-list'):
            apartment = ApartmentItem()
            apartment['service'] = 'gohome'
            apartment['code'] = item.css('tr::attr(data-ad-id)').get()
            apartment['date'] = remove_tags(
                response.css(f'table tbody tr[data-ad-id="{apartment["code"]}"] .td-date').get(),
            ).split(':')[-1].strip()
            apartment['title'] = item.css('.td-name .name__link span::text').get().strip()
            apartment['rooms'] = item.css('.td-name .rooms-count::text').get().strip().split('-')[0]
            apartment['floor'] = '{0}/{2}'.format(*item.css('.td-floor b::text').get().strip().split(' '))
            apartment['url'] = f"https://gohome.by{item.css('.td-name .name__link::attr(href)').get()}"
            apartment['price'] = item.css('.td-price.total .small::text').get().strip()

            yield apartment
