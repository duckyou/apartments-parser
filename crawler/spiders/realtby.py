# -*- coding: utf-8 -*-

import scrapy
from scrapy.http import Response
from w3lib.html import remove_tags

from ..items import ApartmentItem


class RealtbySpider(scrapy.Spider):
    name = 'realtby'

    start_urls = [
        'https://realt.by/mogilev-region/rent/flat-for-long/?search=eJxNjbEOAiEQRL%2FmrCG6UNkY%2F2MDy9xJcxAWNf69XCywm5n3kunlvXNOC92w0P1K1tGJFf1ZxxRUOEFlkNHMIdhJtbTO8fMHU%2BjghlfWXPYpNghXNK5h%2B52YyR7HICYaMk5A3pKnSKtHAuwFbkQ6fwH0ezNu'
    ]

    def parse(self, response: Response):
        for item in response.css('.bd-item'):
            apartment = ApartmentItem()
            apartment['service'] = 'realt'
            apartment['code'] = item.css('.bd-item-right div.bd-item-right-top p::text')[1].get().split(':')[-1].strip()
            apartment['date'] = item.css('.bd-item-right div.bd-item-right-top p::text')[0].get().split(':')[-1].strip()
            apartment['title'] = remove_tags(item.css('.title a::attr(title)')[0].get())
            apartment['rooms'] = item.css('.title strong::text').get().strip()
            apartment['floor'] = None # item.css('.bd-item-right .bd-item-right-center p').get()
            apartment['url'] = item.css('.title a::attr(href)')[0].get()
            apartment['price'] = item.css('.bd-item-left-bottom .price-byr::text').get().strip().split('\xa0')[0]

            yield apartment
