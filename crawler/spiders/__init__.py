# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

from .gohomeby import GohomebySpider
from .kufarby import KufarbySpider
from .realtby import RealtbySpider
