# -*- coding: utf-8 -*-

import json

import scrapy
from scrapy.http import Response

from ..items import ApartmentItem


class KufarbySpider(scrapy.Spider):
    name = 'kufarby'

    start_urls = [
        'https://re.kufar.by/api/search/ads-search/v1/engine/v1/search/raw?cat=1010&typ=let&sort=lst.d&size=20&cur=BYR&gts=o-mogiljovskaja-oblast-c-mogiljov&rnt=1',
    ]

    def parse(self, response: Response):
        items = json.loads(response.text)['ads']

        for item in items:
            apartment = ApartmentItem()
            apartment['service'] = 'kufar'
            apartment['code'] = str(item.get('ad_id'))
            apartment['date'] = item.get('list_time')
            apartment['title'] = item.get('address')
            apartment['rooms'] = item.get('rooms')
            apartment['floor'] = item.get('floor')
            apartment['url'] = item.get('ad_link')
            apartment['price'] = item.get('price_byn')

            yield apartment
