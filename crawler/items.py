# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ApartmentItem(scrapy.Item):
    service = scrapy.Field()
    code = scrapy.Field()
    date = scrapy.Field()
    floor = scrapy.Field()
    rooms = scrapy.Field()
    price = scrapy.Field()
    title = scrapy.Field()
    url = scrapy.Field()
