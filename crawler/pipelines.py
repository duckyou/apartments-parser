# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import hashlib
import time

from scrapy.exceptions import DropItem
from telegram import Bot

from db import apartments_db, Apartment

from .items import ApartmentItem


class SaveApartmentPipeline(object):
    def process_item(self, item: ApartmentItem, spider):
        apartment_hash = hashlib.sha256((item['code'] + item['date']).encode()).hexdigest()

        if apartments_db.search(Apartment.hash == apartment_hash):
            raise DropItem(f"[{item['title']}] already exists")

        apartments_db.insert({'hash': apartment_hash, **item})

        return item


class TelegramNotifyPipeline(object):

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            bot_token=crawler.settings.get('TELEGRAM_BOT_TOKEN'),
            chat_id=crawler.settings.get('TELEGRAM_CHAT_ID'),
        )

    def __init__(self, bot_token, chat_id):
        self.bot = Bot(bot_token)
        self.chat_id = chat_id

    def process_item(self, item: ApartmentItem, spider):
        self.bot.send_message(chat_id=self.chat_id, text=item['url'])
        time.sleep(1)
        return item
